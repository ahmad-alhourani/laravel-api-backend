<?php

use Illuminate\Http\Request;



Route::group(['middleware' => 'cors'], function() {
    Route::get('centers', 'CenterRESTAPIController@index');
    Route::get('centers/{id}', 'CenterRESTAPIController@show');
    Route::post('centers', 'CenterRESTAPIController@store');
    Route::put('centers/{id}', 'CenterRESTAPIController@update');
    Route::delete('centers/{id}', 'CenterRESTAPIController@delete');



    Route::get('students', 'StudentRESTAPIController@index');
    Route::get('students/{id}', 'StudentRESTAPIController@show');
    Route::post('students', 'StudentRESTAPIController@store');
    Route::put('students/{id}', 'StudentRESTAPIController@update');
    Route::delete('students/{id}', 'StudentRESTAPIController@delete');
});

