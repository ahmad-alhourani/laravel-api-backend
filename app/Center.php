<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    protected $fillable = [
        'name',
        'created_by',
        'status',
        'updated_date',
        'created_date',




    ];

    public $timestamps = false;
    protected $table="centers";
//    public $timestamps = ["created_at", "updated_at"];
}
