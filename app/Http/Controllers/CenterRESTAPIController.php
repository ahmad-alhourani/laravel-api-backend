<?php

namespace App\Http\Controllers;

use App\Center;

use Illuminate\Http\Request;

class CenterRESTAPIController extends Controller
{
    public function index()
    {
        return Center::all()->toJson();
    }

    public function show($id)
    {
        return Center::find($id)->toJson();
    }

    public function store(Request $request)
    {


//        error_log('Some message here.');


        $obj = new Center([
            'name' => $request->get('name'),
            'created_by' => 1,
            'created_date' => date("Y-m-d H:i:s"),
            'status' => $request->get('status'),

        ]);
        $obj->save();

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $center = Center::findOrFail($id);
        $center->update($request->all());
        return response()->json($center, 200);
    }

    public function delete($id)
    {

        $obj = Center::find($id);
        if ($obj) {
            $obj->delete();
        }
        return response()->json(null, 204);
    }


}
