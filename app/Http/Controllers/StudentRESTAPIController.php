<?php

namespace App\Http\Controllers;

use App\Student;

use Illuminate\Http\Request;

class StudentRESTAPIController extends Controller
{
    public function index()
    {
        return Student::all()->toJson();
    }

    public function show($id)
    {
        return Student::find($id)->toJson();
    }

    public function store(Request $request)
    {

        error_log( $request->get('birth_date'));
        $obj = new Student([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'status' => $request->get('status'),
            'birth_date' => $request->get('birth_date'),


        ]);
        $obj->save();

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $center = Student::findOrFail($id);
        $center->update($request->all());
        return response()->json($center, 200);
    }

    public function delete($id)
    {

        $obj = Student::find($id);
        if ($obj) {
            $obj->delete();
        }
        return response()->json(null, 204);
    }


}
